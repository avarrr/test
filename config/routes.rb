Rails.application.routes.draw do
	root 'pages#home'

  get "gaming" => "pages#gaming", as: :gaming

  get "sports" => "pages#sports", as: :sports

  get "contact" => "pages#contact", as: :contact

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
